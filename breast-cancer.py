# dataset
from sklearn.datasets import load_breast_cancer

# testing and tuning hyperparameters
from sklearn.model_selection import train_test_split

# models
from sklearn.cluster import KMeans            # https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
from sklearn.mixture import GaussianMixture   # https://scikit-learn.org/stable/modules/generated/sklearn.mixture.GaussianMixture.html
from sklearn.neural_network import MLPClassifier

# yellowbrick
from yellowbrick.cluster import KElbowVisualizer
from yellowbrick.model_selection import LearningCurve

import time
import numpy as np
import itertools
import scipy

from scipy import linalg
import matplotlib.pyplot as plt
import matplotlib as mpl
from sklearn.metrics import mean_squared_error

# dimensionality reduction algorithms
from yellowbrick.features import PCA
from sklearn.decomposition import FastICA
from sklearn.random_projection import GaussianRandomProjection
from yellowbrick.features import RadViz

# global variables
random_state = 55
dataset_name = 'breast-cancer'
feature_names = [
    'mean radius',
    'mean texture',
    'mean perimeter',
    'mean area',
    'mean smoothness',
    'mean compactness',
    'mean concavity',
    'mean concave points',
    'mean symmetry',
    'mean fractal dimension',
    'radius error',
    'texture error',
    'perimeter error',
    'area error',
    'smoothness error',
    'compactness error',
    'concavity error',
    'concave points error',
    'symmetry error',
    'fractal dimension error',
    'worst radius',
    'worst texture',
    'worst perimeter',
    'worst area',
    'worst smoothness',
    'worst compactness',
    'worst concavity',
    'worst concave points',
    'worst symmetry',
    'worst fractal dimension'
]
target_names = ['malignant', 'benign']

# using dataset
# print(load_breast_cancer())
X, y = load_breast_cancer(return_X_y=True)

# split dataset for cluster problem (stratify tries to get equal number of each target class)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, stratify=y, random_state=random_state)

def get_chart_outpath(chart_name):
  return f'charts/{dataset_name}/{chart_name}.png'

def kmeans(train_x, metric='distortion', run='1'):
  model = KMeans(random_state=random_state)
  visualizer = KElbowVisualizer(
    model,
    k=(2,11), # "check_number_of_labels" error when starting with k = 1
    metric=metric,
    timings=False
  )

  visualizer.fit(train_x)                   # Fit the data to the visualizer

  outpath = get_chart_outpath(chart_name=f'run-{run}_kmeans_{metric}')
  visualizer.show(outpath=outpath)    # Finalize and render the figure
  visualizer.poof(clear_figure=True)  # reset plot

def gmm(train_x, test_x, run='1', winner='2'):
  # https://scikit-learn.org/stable/auto_examples/mixture/plot_gmm_selection.html#sphx-glr-auto-examples-mixture-plot-gmm-selection-py
  lowest_bic = np.infty
  bic = []
  n_components_range = range(1, 7)
  # n_components_range = range(1, 30)
  cv_types = ['spherical', 'tied', 'diag', 'full']
  for cv_type in cv_types:
      for n_components in n_components_range:
          # Fit a Gaussian mixture with EM
          gmm = GaussianMixture(
            n_components=n_components,
            covariance_type=cv_type,
            random_state=random_state
          )

          gmm.fit(train_x)
          gmmbic = gmm.bic(train_x)
          # print(gmmbic)
          # bic.append(gmm.bic(train_x))
          bic.append(gmmbic)
          if bic[-1] < lowest_bic:
              lowest_bic = bic[-1]
              best_gmm = gmm

  print(f'lowest_bic: {lowest_bic}')

  bic = np.array(bic)
  color_iter = itertools.cycle(['navy', 'turquoise', 'cornflowerblue',
                                'darkorange'])
  clf = best_gmm
  bars = []

  # Plot the BIC scores
  plt.figure(figsize=(8, 6))
  spl = plt.subplot(2, 1, 1)
  for i, (cv_type, color) in enumerate(zip(cv_types, color_iter)):
      xpos = np.array(n_components_range) + .2 * (i - 2)
      bars.append(plt.bar(xpos, bic[i * len(n_components_range):
                                    (i + 1) * len(n_components_range)],
                          width=.2, color=color))
  plt.xticks(n_components_range)
  plt.ylim([bic.min() * 1.01 - .01 * bic.max(), bic.max()])
  plt.title(f'[{dataset_name}] BIC score per model')
  xpos = np.mod(bic.argmin(), len(n_components_range)) + .65 +\
      .2 * np.floor(bic.argmin() / len(n_components_range))
  plt.text(xpos, bic.min() * 0.97 + .03 * bic.max(), '*', fontsize=14)
  spl.set_xlabel('Number of components')
  spl.legend([b[0] for b in bars], cv_types)

  # Plot the winner
  splot = plt.subplot(2, 1, 2)
  Y_ = clf.predict(test_x)
  for i, (mean, cov, color) in enumerate(zip(clf.means_, clf.covariances_,
                                             color_iter)):
      v, w = linalg.eigh(cov)
      if not np.any(Y_ == i):
          continue
      plt.scatter(test_x[Y_ == i, 0], test_x[Y_ == i, 1], s=4, color=color)

      # Plot an ellipse to show the Gaussian component
      angle = np.arctan2(w[0][1], w[0][0])
      angle = 180. * angle / np.pi  # convert to degrees
      v = 2. * np.sqrt(2.) * np.sqrt(v)
      ell = mpl.patches.Ellipse(mean, v[0], v[1], 180. + angle, color=color)
      ell.set_clip_box(splot.bbox)
      ell.set_alpha(.5)
      splot.add_artist(ell)

  plt.xticks(())
  plt.yticks(())
  plt.title(f'[{dataset_name}] Selected GMM: full model, {winner} components')
  plt.subplots_adjust(hspace=.35, bottom=.02)
  plt.savefig(get_chart_outpath(f'run-{run}_gmm'))
  plt.close()

def pca(train_x, n_components=2, feature_names=feature_names, run='1'):
  heatmap=True

  if n_components == 3:
    heatmap=False

  visualizer = PCA(
    scale=True,
    proj_features=False,
    heatmap=heatmap,
    features=feature_names,
    classes=target_names,
    projection=n_components,
    random_state=random_state
  )

  visualizer.fit_transform(train_x, y_train)

  outpath = get_chart_outpath(chart_name=f'run-{run}_pca_{n_components}-components')
  visualizer.show(outpath=outpath)    # Finalize and render the figure
  visualizer.poof(clear_figure=True)  # reset plot

# largest avg kurtosis = 52.17 when n_components = 3
def ica(train_x, algorithm='parallel', feature_names=feature_names, run='1'):
  # try from 1 to number of total features
  n_components_list = range(1,len(feature_names) + 1)
  k_list = []

  for i in n_components_list:
    n_components = i;

    ica = FastICA(
      n_components=n_components,  # use None to default to all components
      algorithm=algorithm, # parallel, deflation
      max_iter=2000, # must be high to converge at low n_components like 1-2
      random_state=random_state
    )

    ica_result = ica.fit_transform(train_x)

    # https://piazza.com/class/kdx36x23bcer4?cid=679
    k = scipy.stats.kurtosis(ica_result, fisher=True).mean()
    # print(k)
    k_list.append(k)

  # print(k_list)
  # print(len(k_list))

  # plot
  plt.plot(np.arange(1,len(feature_names) + 1), np.array(k_list))
  plt.xlabel('Components')
  plt.ylabel('Average Kurtosis (fisher)')
  plt.xticks(np.arange(1,len(feature_names) + 1))

  outpath = get_chart_outpath(chart_name=f'run-{run}_ica_kurtosis_1-{len(feature_names)}-components')
  plt.savefig(outpath)
  plt.close()

# random projection
# gains on minimizing reconstruction error plateu around 21 components, suggesting 8-9 features/dimensions are kinda useless
def rp(train_x, feature_names=feature_names, run='1'):
  # try from 1 to number of total features
  n_components_list = range(1,len(feature_names) + 1)
  error_list = [] # reconstruction error

  for i in n_components_list:
    n_components = i;
    # https://scikit-learn.org/stable/modules/generated/sklearn.random_projection.GaussianRandomProjection.html
    transformer = GaussianRandomProjection(
      n_components=n_components,
      random_state=random_state
    )

    X_new = transformer.fit_transform(train_x)

    # print(train_x.shape)
    # print(X_new.shape)

    # https://piazza.com/class/kdx36x23bcer4?cid=679
    inverse_data = np.linalg.pinv(transformer.components_.T)
    reconstructed_data = X_new.dot(inverse_data)
    # print(reconstructed_data)
    # print(len(reconstructed_data))

    error = mean_squared_error(reconstructed_data, train_x)
    # print(error)
    error_list.append(error)

  # plot
  plt.plot(np.arange(1,len(feature_names) + 1), np.array(error_list))
  plt.xlabel('Components')
  plt.ylabel('Reconstruction Error') # mean squared error
  plt.xticks(np.arange(1,len(feature_names) + 1))

  outpath = get_chart_outpath(chart_name=f'run-{run}_grp_reconstruction-error_1-{len(feature_names)}-components')
  plt.savefig(outpath)
  plt.close()

# https://www.scikit-yb.org/en/latest/api/features/radviz.html
def radvis(train_x, feature_names=feature_names, run='1'):

  visualizer = RadViz(
    features=feature_names,
    classes=target_names,
    random_state=random_state
  )

  visualizer.fit(train_x, y_train)           # Fit the data to the visualizer
  visualizer.transform(train_x)        # Transform the data

  # plot
  outpath = get_chart_outpath(chart_name=f'run-{run}_radvis')
  visualizer.show(outpath=outpath)    # Finalize and render the figure
  visualizer.poof(clear_figure=True)  # reset plot

# part 4 stuff
def learning_curve(X, y, model, chart_name='neural-network'):
  '''
  This method generates a Learning Curve chart for a given model.
  It runs in ~0.9 seconds for Decision Tree
    Finished in [0.8959] seconds
    Finished in [0.9006] seconds
  '''
  print(f'running learning_curve [{chart_name}]')  

  cv = 5  # cross validation folds
  sizes = np.linspace(0.1, 1.0, 5)
  # sizes = np.linspace(0.1, 1.0, 10)
  # sizes = [30, 60, 90, 120]

  visualizer = LearningCurve(
    model,
    cv=cv,
    scoring='f1_weighted',
    train_sizes=sizes,
    n_jobs=5,
    random_state=random_state
  )

  # start time
  start = time.time()

  # Fit the training data to the visualizer
  # visualizer.fit(X_train, y_train)
  visualizer.fit(X, y)

  # end time
  elapsed = round(time.time() - start, 4)  # round to 4 places
  print(f'Finished in [{elapsed}] seconds')

  # Draw visualization
  outpath = get_chart_outpath(f'{chart_name}_learning-curve')
  visualizer.show(outpath=outpath)

  # reset plot
  visualizer.poof(clear_figure=True)

# nn from project 1
def nn_tuned():
  '''
  GridSearchCV best params:
  {'alpha': 0.001, 'hidden_layer_sizes': 100, 'max_iter': 4000, 'solver': 'lbfgs'}
  '''
  model = MLPClassifier(
    solver='lbfgs',
    alpha=0.001,
    hidden_layer_sizes=(100),
    max_iter=4000,
    random_state=random_state
  )

  return model

if __name__ == '__main__':
  print(f'Executing code for dataset [{dataset_name}]')

  # === part 1 ===

  kmeans(X_train, 'distortion')
  kmeans(X_train, 'silhouette')
  kmeans(X_train, 'calinski_harabasz')

  # 2 components, full covariance: lowest BIC score = -28906.24
  gmm(X_train, X_test)

  # === part 2 ===

  pca(X_train, n_components=2, feature_names=feature_names)
  
  ica(X_train, feature_names=feature_names)
  
  rp(X_train, feature_names=feature_names)

  radvis(X_train, feature_names=feature_names)

  # === part 3 ===
  
  # TODO: rerun all, select features using PCA plot
  # best features are:
    # mean fractal dimension (1st best by far)
    # fractal dimension error (2nd best)
    # worst fractal dimension (3rd best)
    # mean concavity (all concave are 4th best)
    # mean concave points
    # worst concavity
    # worst concave points

  # the bad/noisy features are:
    # mean radius
    # mean area
    # worst area
    # mean perimeter
    # worst radius
    # worst perimeter
    # area error
    # radius error
    # perimeter error
    # 

  # feature_names = [
  #     'mean radius',             # 0
  #     'mean texture',            # 1
  #     'mean perimeter',          # 2
  #     'mean area',               # 3
  #     'mean smoothness',         # 4
  #     'mean compactness',        # 5
  #     'mean concavity',          # 6
  #     'mean concave points',     # 7
  #     'mean symmetry',           # 8
  #     'mean fractal dimension',  # 9
  #     'radius error',            # 10
  #     'texture error',           # 11
  #     'perimeter error',         # 12
  #     'area error',              # 13
  #     'smoothness error',        # 14
  #     'compactness error',       # 15
  #     'concavity error',         # 16
  #     'concave points error',    # 17
  #     'symmetry error',          # 18
  #     'fractal dimension error', # 19
  #     'worst radius',            # 20
  #     'worst texture',           # 21
  #     'worst perimeter',         # 22
  #     'worst area',              # 23
  #     'worst smoothness',        # 24
  #     'worst compactness',       # 25
  #     'worst concavity',         # 26
  #     'worst concave points',    # 27
  #     'worst symmetry',          # 28
  #     'worst fractal dimension'  # 29
  # ]

  print(X_train.shape)

  # make copies of train and test data and delete unwanted features
  X_train2 = np.copy(X_train)
  X_test2 = np.copy(X_test)

  unwanted_feature_indexes = [
    0,
    1,
    2,
    3,
    4,
    5,
    # 6,
    # 7,
    8,
    # 9, 
    10,
    11,
    12,
    13,
    14,
    15,
    # 16,
    # 17,
    18,
    # 19,
    20,
    21,
    22,
    23,
    24,
    25,
    # 26,
    # 27,
    28,
    # 29
  ]

  # start with highest number index to avoid issue with deletion in place changing the indexes
  for i in reversed(unwanted_feature_indexes):
    X_train2 = np.delete(X_train2, i, 1)
    X_test2 = np.delete(X_test2, i, 1)

  print(X_train2.shape)
  print(X_train[-1])
  print(X_train2[-1])

  # run everything again with less dimensions/features

  kmeans(X_train2, 'distortion', run='2')
  kmeans(X_train2, 'silhouette', run='2')
  kmeans(X_train2, 'calinski_harabasz', run='2')

  # 4 components, full covariance: lowest BIC score = -26420.88
  gmm(X_train2, X_test2, run='2', winner='4')

  # === dimensionality  ===

  feature_names2 = [
      'mean concavity',          # 6
      'mean concave points',     # 7
      'mean fractal dimension',  # 9
      'concavity error',         # 16
      'concave points error',    # 17
      'fractal dimension error', # 19
      'worst concavity',         # 26
      'worst concave points',    # 27
      'worst fractal dimension'  # 29
  ]

  pca(X_train2, n_components=2, run='2', feature_names=feature_names2)
  
  ica(X_train2, run='2', feature_names=feature_names2)
  
  rp(X_train2, run='2', feature_names=feature_names2)

  radvis(X_train2, run='2', feature_names=feature_names2)


  # === part 4 ===
  
  # takes ~2.6 seconds to fit vs original project1 ~1.8 seconds
  learning_curve(X_train2, y_train, nn_tuned(), chart_name='nn-tuned')
