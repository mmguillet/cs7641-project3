# CS7641 Project 3
Matthew Guillet

Code: https://bitbucket.org/mmguillet/cs7641-project3/src/master/

## Install and Run
```
# clone repo
git clone git@bitbucket.org:mmguillet/cs7641-project3.git

# go into project directory
cd cs7641-project3

# install python virtual environment
sudo apt install python3-venv

# setup new python virtual environment
python3 -m venv venv

# activate virtual environment
source venv/bin/activate

# update python module installer modules
pip install -U setuptools pip

# install python modules
pip install -r requirements.txt

# run code and saves all charts
python3 iris.py && python3 breast-cancer.py
```

## Cite Sources
* https://scikit-learn.org
* https://www.scikit-yb.org
* https://stackoverflow.com
